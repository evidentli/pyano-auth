from abc import ABCMeta, abstractmethod
from pymongo import MongoClient
from flask_login import UserMixin, AnonymousUserMixin
from datetime import datetime
from copy import deepcopy
from time import sleep
import requests
import os


class UserDaoException(Exception):
    pass


class User(UserMixin):

    def __init__(self, dao, user_id, user_attributes=None):
        self.dao = dao

        self.id = None
        self.first_name = None
        self.last_name = None
        self.roles = []

        if isinstance(user_id, str):
            user_id = user_id.lower()

        self.id = str(user_id)
        user = {}
        if dao:
            user = self.dao.get_user(user_id)
        if not user:
            user = user_attributes
        elif user_attributes:
            for k, v in user_attributes.items():
                if v:
                    user[k] = v

        if not user:
            user = {}

        self.email = user.get('email')
        self.first_name = user.get('first_name')
        self.last_name = user.get('last_name')
        self.roles = user.get('roles', [])
        self.last_login_date = datetime.now().isoformat()

        self.update()

    def update(self):
        if not self.dao:
            return False
        user = {
            "_id": self.id,
            "email": self.email,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "roles": self.roles,
            "last_login_date": self.last_login_date
        }
        return self.dao.update_user(user)


class AnonymousUser(AnonymousUserMixin, User):

    def __init__(self):
        User.__init__(self, None, None)
        AnonymousUserMixin.__init__(self)
        self.authenticated = False

    @property
    def is_authenticated(self):
        return self.authenticated


class UserDao(metaclass=ABCMeta):
    def __init__(self):
        pass

    @abstractmethod
    def get_user(self, user_id):
        pass

    @abstractmethod
    def update_user(self, user):
        pass


class PianoUserDao(UserDao):

    PIANO_API = os.getenv("PIANO_API")
    REQUEST_RETRY = int(os.getenv("REQUEST_RETRY", 3))
    REQUEST_RETRY_WAIT = float(os.getenv("REQUEST_RETRY_WAIT", 0.5))

    def __init__(self, api_url=None):
        super(PianoUserDao, self).__init__()
        if api_url:
            self.PIANO_API = api_url

    def piano_get(self, url, params=None):
        url = self.PIANO_API + url
        retry = 1
        while retry <= self.REQUEST_RETRY:
            try:
                if params:
                    return requests.get(url, params=params)
                return requests.get(url)
            except requests.ConnectionError:
                retry += 1
                sleep(self.REQUEST_RETRY_WAIT)

        raise UserDaoException("Could not reach Piano API at GET %s" % url)

    def piano_post(self, url, json_data=None, headers=None):
        url = self.PIANO_API + url

        req_headers = {}
        if headers:
            req_headers.update(headers)

        retry = 1
        while retry <= self.REQUEST_RETRY:
            try:
                if json_data:
                    return requests.post(url, json=json_data, headers=headers)
                return requests.post(url, headers=headers)
            except requests.ConnectionError:
                retry += 1
                sleep(self.REQUEST_RETRY_WAIT)

        raise UserDaoException("Could not reach Piano API at POST %s" % url)

    def get_user(self, user_id):
        if user_id is not None:
            response = self.piano_get("/users/%s" % user_id)
            if response.status_code == requests.codes.ok and response.content:
                try:
                    user = response.json()
                    return user
                except Exception as e:
                    pass
        return None

    def update_user(self, user):
        if "_id" not in user or user["_id"] is None:
            raise UserDaoException("User must have an id assigned")

        response = self.piano_post("/users", json_data=[user])
        if response.status_code == requests.codes.ok and response.content:
            try:
                user_id = response.json()[0]
                return user_id
            except Exception as e:
                raise UserDaoException("User was not inserted/updated: " +str(e))
        else:
            raise UserDaoException("User was not inserted/updated: " + str(response.status_code) + " " + response.content)


class MongoUserDao(UserDao):

    def __init__(self, server, port, database="users"):
        super(UserDao, self).__init__()
        if not server:
            raise RuntimeError()
        try:
            port = int(port)
        except ValueError:
            port = None
        if not port:
            raise RuntimeError()

        try:
            client = MongoClient(server, port, connect=False)
            db = client[database]
            self.table = db.users
        except Exception as e:
            raise RuntimeError("Error during database connection: %s" % str(e))

    def update_user(self, user):
        user = deepcopy(user)

        if "_id" not in user or user["_id"] is None:
            raise UserDaoException("User must have an id assigned")

        user_id = user.pop("_id")
        try:
            result = self.table.update({"_id": user_id}, {"$set": user}, upsert=True)
        except Exception as e:
            raise UserDaoException("User was not inserted/updated: " + str(e))

        if result:
            return user_id
        else:
            raise UserDaoException("User was not inserted/updated")

    def get_user(self, user_id):
        if user_id is not None:
            return self.table.find_one({"_id": user_id})
        return None


def user_dao(dao_type, **kwargs):
    if dao_type == "api":
        if "api_url" in kwargs and kwargs["api_url"]:
            return PianoUserDao(kwargs["api_url"])
        elif os.getenv("PIANO_API"):
            return PianoUserDao()
    elif dao_type.lower() == "mongodb":
        # TODO update to work with authentication
        host = kwargs.get("host")
        port = kwargs.get("port")
        db = kwargs.get("database")
        if host and port and db:
            return MongoUserDao(host, port, db)
        else:
            raise UserDaoException("Missing parameters for %s Database Type: host, port and database are required" % dao_type)
    raise UserDaoException("Invalid Database Type: %s is not supported" % dao_type)
