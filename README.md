# Pyano-Auth

PianoAuth is a python package that can be used to add authentication to your Flask based, Piano integrated application.

### Tech

PianoAuth uses some open source projects to work properly:

* [Python] - programming language
* [Flask] - a microframework for Python
* [Flask-Login] - provides user session management for Flask
* [Flask-CAS] - a Flask extension which makes it easy to authenticate with a CAS Server
* [pysaml2] - a pure python implementation of SAML2

### Installation

##### Install required libraries:
Mac OSX: 
```sh
brew install libffi libxmlsec1
```

RHEL:
```sh
sudo yum install libffi-devel xmlsec1 xmlsec1-openssl
```

##### Install PianoAuth:
```sh
$ pip install git+https://bitbucket.org/evidentli/pyano-auth.git
```

### Configuration

| Application Setting      | Required | Description |
| ------------------------ | -------- | ----------- |
| PIANO_AUTH_TYPE          | True     | Type of authentication (SAML, CAS) |
| PIANO_AUTH_IDP_URLS      | True     | Identity provider name / url map |
| PIANO_AUTH_AFTER_LOGIN   |          | Route name to redirect to after successful login |
| PIANO_AUTH_AFTER_LOGOUT  |          | Route name to redirect to after logout |
| PIANO_AUTH_SAML_METADATA |          | Metadata text (xml). Only applicable for SAML auth type |
| PIANO_AUTH_USER_API      |          | Piano API to use for storing/retrieving IdP user information |
| PIANO_AUTH_USER_DB_TYPE  |          | User DB type to use for storing/retrieving IdP user information |
| PIANO_AUTH_USER_DB_HOST  |          | User DB host for PIANO_AUTH_DB_TYPE |
| PIANO_AUTH_USER_DB_PORT  |          | User DB port for PIANO_AUTH_DB_TYPE |
| PIANO_AUTH_USER_DB_NAME  |          | User DB name for PIANO_AUTH_DB_TYPE |


### Use

##### Initialisation
```python
from piano_auth.authentication import piano_authentication
app = Flask(__name__)
app.config.update(
    PIANO_AUTH_TYPE = "CAS",
    PIANO_AUTH_IDP_URLS = {'cas': 'http://cas.example.com'},
    PIANO_AUTH_AFTER_LOGIN = "home_page",
)
piano_auth = piano_authentication(app)
```

##### Functionality
```python

from app import app, piano_auth

login_required = piano_auth.login_required

@app.route("/")
@login_required
def home_page():
    return "Welcome to my home page! User ID: %s" % piano_auth.user_id()
```

License
----
MIT

Please also see [Flask-CAS LICENSE](LICENSE-Flask-CAS)


## Authors

* [Evidentli](https://evidentli.com/)


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [PianoAuth]: <https://git.aihi.mq.edu.au/evidence/PianoAuth>
   [Python]: <https://www.python.org/>
   [Flask]: <http://flask.pocoo.org/>
   [Flask-Login]: <http://flask-login.readthedocs.io/en/latest/>
   [Flask-CAS]: <https://github.com/cameronbwhite/Flask-CAS>
   [pysaml2]: <https://pysaml2.readthedocs.io>

