import requests
from flask import url_for, session, request, redirect
from flask_login import LoginManager, current_user as saml_current_user, login_required as saml_login_required, \
    logout_user as saml_logout_user, login_user as saml_login_user
from saml2 import BINDING_HTTP_REDIRECT, BINDING_HTTP_POST, entity
from saml2.client import Saml2Client
from saml2.config import Config as Saml2Config
from werkzeug.utils import redirect

from piano_auth import PianoAuth, PianoRoles
from piano_users.user_dao import User, AnonymousUser


class PianoSAML(PianoAuth, LoginManager):

    # SAML/Okta integration code taken and adapted from https://github.com/jpf/okta-pysaml2-example.git

    saml_metadata = {}

    def __init__(self, app, idp_urls=None, *args, **kwargs):
        PianoAuth.__init__(self, app, idp_urls)
        LoginManager.__init__(self)
        self.init_app(app)

        if "PIANO_AUTH_SAML_METADATA" in app.config:
            self.saml_metadata.update(app.config["PIANO_AUTH_SAML_METADATA"])

        @self.user_loader
        def load_user(user_id):
            return User(self.user_dao, user_id)

        @self.unauthorized_handler
        def unauthorized():
            return redirect(url_for("piano_login", idp_name=list(self.idp_urls.keys())[0]))

        @app.route("/saml/sso/<idp_name>", methods=['POST'])
        def piano_sso_login(idp_name):
            return self.idp_login(idp_name)

        @app.route("/saml/login/<idp_name>")
        def piano_login(idp_name):
            return self.service_provider_login(idp_name)

        self.anonymous_user = AnonymousUser

    def current_user(self):
        return saml_current_user

    def user_id(self):
        user_id = session.get("uid")
        if not user_id:
            user_id = session.get("user_id")
        if not user_id:
            user_id = self.user_email()
        if isinstance(user_id, list):
            user_id = user_id[0]
        if isinstance(user_id, str):
            return user_id.lower()
        return user_id

    def user_email(self):
        return session.get(self.user_email_field)

    def authenticate_anonymous_user(self, admin=False):
        self.current_user().__setattr__("authenticated", True)
        if admin:
            self.current_user().__setattr__("roles", [PianoRoles.admin.value])

    def admin_user(self):
        return PianoRoles.admin.value in saml_current_user.roles

    def login_required(self, func):
        return saml_login_required(func)

    def logout(self):
        return saml_logout_user()

    def saml_client_for(self, idp_name=None):
        """
        Given the name of an IdP, return a configuration.
        The configuration is a hash for use by saml2.config.Config
        """

        if idp_name not in self.idp_urls:
            raise Exception("Settings for IDP '{}' not found".format(idp_name))
        acs_url = url_for(
            "piano_sso_login",
            idp_name=idp_name,
            _external=True)
        https_acs_url = url_for(
            "piano_sso_login",
            idp_name=idp_name,
            _external=True,
            _scheme='https')

        if self.saml_metadata and idp_name in self.saml_metadata:
            metadata = self.saml_metadata[idp_name]
        else:
            #   SAML metadata changes very rarely. On a production system,
            #   this data should be cached as appropriate for your production system.
            rv = requests.get(self.idp_urls[idp_name])
            metadata = rv.text

        settings = {
            'metadata': {
                'inline': [metadata],
            },
            'service': {
                'sp': {
                    'endpoints': {
                        'assertion_consumer_service': [
                            (acs_url, BINDING_HTTP_REDIRECT),
                            (acs_url, BINDING_HTTP_POST),
                            (https_acs_url, BINDING_HTTP_REDIRECT),
                            (https_acs_url, BINDING_HTTP_POST)
                        ],
                    },
                    # Don't verify that the incoming requests originate from us via
                    # the built-in cache for authn request ids in pysaml2
                    'allow_unsolicited': True,
                    # Don't sign authn requests, since signed requests only make
                    # sense in a situation where you control both the SP and IdP
                    'authn_requests_signed': False,
                    'logout_requests_signed': True,
                    'want_assertions_signed': True,
                    'want_response_signed': False,
                },
            },
        }
        spConfig = Saml2Config()
        spConfig.load(settings)
        spConfig.allow_unknown_attributes = True
        saml_client = Saml2Client(config=spConfig)
        return saml_client

    def idp_login(self, idp_name):
        saml_client = self.saml_client_for(idp_name)
        authn_response = saml_client.parse_authn_request_response(
            request.form['SAMLResponse'],
            entity.BINDING_HTTP_POST)
        authn_response.get_identity()
        user_info = authn_response.get_subject()
        username = user_info.text

        # This is what as known as "Just In Time (JIT) provisioning".
        # What that means is that, if a user in a SAML assertion
        # isn't in the user store, we create that user first, then log them in
        user_attributes = {
            'first_name': authn_response.ava.get(self.user_first_name_field, [None])[0],
            'last_name': authn_response.ava.get(self.user_last_name_field, [None])[0],
            'email': authn_response.ava.get(self.user_email_field, [None])[0],
            'roles': []
        }
        if authn_response.ava.get(self.user_admin_field, [None])[0] == "true":
            user_attributes['roles'] += [PianoRoles.admin.value]
        if "uid" in authn_response.ava and authn_response.ava["uid"]:
            username = authn_response.ava["uid"]
            if isinstance(username, list):
                username = username[0]
        user = User(self.user_dao, username, user_attributes)
        session['saml_attributes'] = authn_response.ava
        saml_login_user(user)
        url = url_for(self.after_login_url)
        # NOTE:
        #   On a production system, the RelayState MUST be checked
        #   to make sure it doesn't contain dangerous URLs!
        # TODO check relay state url
        if 'RelayState' in request.form:
            url = request.form['RelayState']
        return redirect(url)

    def service_provider_login(self, idp_name):
        saml_client = self.saml_client_for(idp_name)
        reqid, info = saml_client.prepare_for_authenticate()

        redirect_url = None
        # Select the IdP URL to send the AuthN request to
        for key, value in info['headers']:
            if key == 'Location':
                redirect_url = value
        response = redirect(redirect_url, code=302)
        # NOTE:
        #   I realize I _technically_ don't need to set Cache-Control or Pragma:
        #     http://stackoverflow.com/a/5494469
        #   However, Section 3.2.3.2 of the SAML spec suggests they are set:
        #     http://docs.oasis-open.org/security/saml/v2.0/saml-bindings-2.0-os.pdf
        #   We set those headers here as a "belt and suspenders" approach,
        #   since enterprise environments don't always conform to RFCs
        response.headers['Cache-Control'] = 'no-cache, no-store'
        response.headers['Pragma'] = 'no-cache'
        return response
