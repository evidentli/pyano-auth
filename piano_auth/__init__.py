from enum import Enum
from abc import ABCMeta, abstractmethod

from piano_auth.authentication import PianoAuthConfigException
from piano_users.user_dao import user_dao


class PianoRoles(Enum):
    admin = "admin"


class PianoAuth(metaclass=ABCMeta):
    idp_urls = {}
    after_login_url = "index"
    after_logout_url = None

    user_first_name_field = "firstname"
    user_last_name_field = "lastname"
    user_email_field = "email"
    user_admin_field = "Admin"

    user_dao = None

    def __init__(self, app, idp_urls=None, *args, **kwargs):
        if "PIANO_AUTH_AFTER_LOGIN" in app.config:
            self.after_login_url = app.config["PIANO_AUTH_AFTER_LOGIN"]

        if "PIANO_AUTH_AFTER_LOGOUT" in app.config:
            self.after_logout_url = app.config["PIANO_AUTH_AFTER_LOGOUT"]

        if "PIANO_AUTH_IDP_URLS" in app.config:
            self.idp_urls = app.config["PIANO_AUTH_IDP_URLS"]
        if idp_urls:
            self.idp_urls = idp_urls

        if not self.idp_urls:
            raise PianoAuthConfigException("PIANO_AUTH_IDP_URLS is not set")

        if "PIANO_AUTH_USER_API" in app.config and app.config["PIANO_AUTH_USER_API"]:
            url = app.config.get("PIANO_AUTH_USER_API", None)
            self.user_dao = user_dao(
                dao_type="api",
                api_url=url
            )
        elif "PIANO_AUTH_USER_DB_HOST" in app.config and "PIANO_AUTH_USER_DB_PORT" in app.config \
                and "PIANO_AUTH_USER_DB_NAME" in app.config:
            self.user_dao = user_dao(
                dao_type=app.config.get("PIANO_AUTH_USER_DB_TYPE", "mongodb"),
                host=app.config["PIANO_AUTH_USER_DB_HOST"],
                port=app.config["PIANO_AUTH_USER_DB_PORT"],
                database=app.config.get("PIANO_AUTH_USER_DB_NAME")
            )

    @abstractmethod
    def login_required(self, func):
        pass

    @abstractmethod
    def current_user(self):
        pass

    @abstractmethod
    def logout(self):
        pass

    @abstractmethod
    def admin_user(self):
        pass

    @abstractmethod
    def user_id(self):
        pass

    @abstractmethod
    def user_email(self):
        pass

    @abstractmethod
    def authenticate_anonymous_user(self, admin=False):
        pass

    def user_roles(self):
        return []


from .piano_cas import PianoCAS
from .piano_saml import PianoSAML
from .piano_keycloak import PianoKeycloakOpenID
