from __future__ import annotations
import os
from dataclasses import dataclass
from functools import wraps, lru_cache
from enum import Enum
import time

import requests
from flask import session, redirect, url_for, request
from authlib.integrations.flask_client import OAuth

from piano_auth import PianoAuth, PianoRoles

CACHE_MAX_SIZE: int = int(os.getenv("PIANO_AUTH_CACHE_MAX_SIZE", 1024))
ANONYMOUS_USER: str = "anonymous"


@dataclass
class User:
    id: str
    email: str
    roles: list[str]


class Session(Enum):
    user = "user"
    access_token = "access_token"
    refresh_token = "refresh_token"
    refreshed_at = "refreshed_at"


class SessionUser(Enum):
    id = "sub"
    email = "email"
    roles = "roles"


class KeycloakTokenResponse(Enum):
    access_token = "access_token"
    refresh_token = "refresh_token"
    not_before = "not-before-policy"
    expires_at = "expires_at"


def _get_time_stamp() -> float:
    return time.mktime(time.localtime())


def _log_error(e: str):
    print(f"PIANO_KEYCLOAK: ERROR: {e}")


class PianoKeycloakOpenID(PianoAuth):

    def __init__(self, app, idp_urls=None, *args, **kwargs):
        PianoAuth.__init__(self, app, idp_urls)

        self.oauth = oauth = OAuth(app)
        idp_id, idp_url = list(self.idp_urls.items())[0]
        self.idp_id = idp_id
        self.issuer = idp_url
        self.client_id = app.config.get("PIANO_AUTH_CLIENT_ID", os.getenv("PIANO_AUTH_CLIENT_ID"))
        self.client_secret = app.config.get("PIANO_AUTH_CLIENT_SECRET", os.getenv("PIANO_AUTH_CLIENT_SECRET"))
        oauth.register(
            name=self.idp_id,
            client_id=self.client_id,
            client_secret=self.client_secret,
            server_metadata_url=f"{self.issuer}/.well-known/openid-configuration",
            client_kwargs={
                'scope': 'openid email profile'
            }
        )

        @app.route(f"/{self.idp_id}/login")
        def piano_keycloak_login():
            redirect_url = "/"
            try:
                token_response = oauth.keycloak.authorize_access_token()
                user = oauth.keycloak.parse_id_token(token_response)
                if user:
                    session[Session.user.value] = {
                        SessionUser.id.value: user[SessionUser.id.value],
                        SessionUser.email.value: user.get(SessionUser.email.value, ""),
                        SessionUser.roles.value: user.get(SessionUser.roles.value, [])
                    }
                    session[Session.access_token.value] = token_response.get(KeycloakTokenResponse.access_token.value)
                    session[Session.refresh_token.value] = token_response.get(KeycloakTokenResponse.refresh_token.value)
                    # expires_at = token_response.get(KeycloakTokenResponse.expires_at.value)
                    session[Session.refreshed_at.value] = _get_time_stamp()
                    session.modified = True
                    _url = request.args.get("piano_redirect_to")    # set in login method below
                    if _url:
                        redirect_url = _url
            except Exception as e:
                _log_error(f"LOGIN: {repr(e)}")
            return redirect(redirect_url)

    @property
    def _user(self) -> User | None:
        """User object for internal use only"""
        if not self.is_logged_in():
            return None
        user = session.get(Session.user.value)
        if user:
            return User(
                user.get(SessionUser.id.value, ""),
                user.get(SessionUser.email.value, ""),
                user.get(SessionUser.roles.value, [])
            )
        return None

    @property
    def user(self) -> dict:
        """User dict (empty if not set)"""
        user = self._user
        if user:
            return {
                "email": user.email,
                "roles": user.roles,
            }
        return {}

    @staticmethod
    def _clear_session() -> None:
        session.clear()

    @staticmethod
    def _minutes_since_token_refreshed() -> float:
        """Minutes (rounded to zero) since token was created/refreshed"""
        refreshed_at = session.get(Session.refreshed_at.value)
        if refreshed_at:
            refreshed_at_time = time.mktime(time.localtime(refreshed_at))
            minutes_since = round(abs(refreshed_at_time - _get_time_stamp()) / 60, 0) + 1
            return minutes_since
        return 0

    @lru_cache(maxsize=CACHE_MAX_SIZE)
    def _get_user_info(self, access_token: str, mins_since: float) -> dict | None:
        """
        Get the user info from Keycloak
        :param access_token: the access token to use
        :param mins_since: used for caching (will cache each request for a minute, per access_token)
        """
        if not access_token:
            return None
        headers = {
            'Authorization': f"Bearer {access_token}",
            'Accept': 'application/json'
        }
        response = requests.post(f'{self.issuer}/protocol/openid-connect/userinfo', headers=headers)
        if response.ok:
            return response.json()
        return None

    def _introspect_token(self, token: str, **kwargs) -> bool:
        """
        Check if token is still active
        :param token: the token to verify (access or refresh)
        """
        data = {
            "client_id": self.client_id,
            "client_secret": self.client_secret,
            "token": token
        }
        try:
            response = requests.post(f"{self.issuer}/protocol/openid-connect/token/introspect", data=data)
            if response.ok:
                _json = response.json()
                return _json.get("active", False)
        except Exception as e:
            _log_error(f"INTROSPECT: {repr(e)}")
        return False

    @lru_cache(maxsize=CACHE_MAX_SIZE)
    def _introspect_token_cache(self, token: str, mins_since: float) -> bool:
        """
        Check if token is still active (with lru_cache)
        :param token: the token to verify (access or refresh)
        :param mins_since: used for caching (will cache each request for a minute, per unique token)
        """
        return self._introspect_token(token, mins_since=mins_since)

    def _refresh_token(self) -> bool:
        """Use refresh_token to get new access token"""
        token = session.get(Session.refresh_token.value)
        if not token:
            return False
        data = {
            "grant_type": "refresh_token",
            "client_id": self.client_id,
            "client_secret": self.client_secret,
            "refresh_token": token,
        }
        try:
            response = requests.post(f"{self.issuer}/protocol/openid-connect/token", data=data)
            if response.ok:
                _json = response.json()
                access_token = _json.get("access_token")
                refresh_token = _json.get("refresh_token")
                session[Session.access_token.value] = access_token
                session[Session.refresh_token.value] = refresh_token
                session[Session.refreshed_at.value] = _get_time_stamp()
                session.modified = True
                return True
            else:
                _log_error(f"REFRESH TOKEN NOT OK: {response.text}")
        except Exception as e:
            _log_error(f"REFRESH TOKEN: {repr(e)}")
        return False

    @staticmethod
    def _is_anonymous_user() -> bool:
        user = session.get(Session.user.value)
        return user and isinstance(user, dict) and user.get(SessionUser.id.value) == ANONYMOUS_USER

    def _verify_token(self):
        access_token = session.get(Session.access_token.value)

        if not access_token:
            return False
        minutes_since_token_refreshed = self._minutes_since_token_refreshed()
        if minutes_since_token_refreshed:   # i.e. non-zero
            if self._introspect_token_cache(access_token, mins_since=minutes_since_token_refreshed):
                # if access token is still valid, we're still logged in
                return True
        else:
            if self._introspect_token(access_token):
                return True
        # otherwise, attempt to regenerate with refresh token, if that fails we're no longer logged in
        return self._refresh_token()

    def is_logged_in(self) -> bool:
        if self._is_anonymous_user():
            return True
        user = session.get(Session.user.value)
        if user and self._verify_token():
            return True
        return False

    def login_required(self, func):
        @wraps(func)
        def wrap(*args, **kwargs):
            if self.is_logged_in():
                return func(*args, **kwargs)
            else:
                return self.login()
        return wrap

    def current_user(self):
        return self.user

    def login(self):
        self.logout()
        redirect_uri = url_for('piano_keycloak_login', piano_redirect_to=request.url, _external=True)
        return self.oauth.keycloak.authorize_redirect(redirect_uri)

    def logout(self):
        try:
            refresh_token = session.get(Session.refresh_token.value)
            if refresh_token:
                end_session_endpoint = f'{self.issuer}/protocol/openid-connect/logout'
                requests.post(end_session_endpoint, data={
                    "client_id": self.client_id,
                    "client_secret": self.client_secret,
                    "refresh_token": refresh_token
                })
        except Exception:
            pass
        self._clear_session()
        return redirect('/')

    def admin_user(self):
        user = self._user
        return PianoRoles.admin.value in user.roles if user else False

    def user_id(self):
        user = self._user
        return user.id if user else None

    def user_email(self):
        user = self._user
        return user.email if user else None

    def user_roles(self):
        user = self._user
        return user.roles if user else []

    def authenticate_anonymous_user(self, admin=False):
        anonymous_user = {
            SessionUser.id.value: ANONYMOUS_USER,
            SessionUser.roles.value: []
        }
        if admin:
            anonymous_user[SessionUser.roles.value] = [PianoRoles.admin.value]
        session[Session.user.value] = anonymous_user
        session.modified = True
