from flask import session

from flask_cas import CAS, login_required as cas_login_required, logout as cas_logout

from piano_auth import PianoAuth, PianoRoles
from piano_users.user_dao import User


class PianoCAS(PianoAuth, CAS):

    # TODO fix integration with user_dao

    user = None

    def __init__(self, app, idp_urls=None, *args, **kwargs):
        PianoAuth.__init__(self, app, idp_urls)

        app.config.update(
            CAS_SERVER=list(self.idp_urls.values())[0],
            CAS_VALIDATE_ROUTE='/cas/p3/serviceValidate',
            CAS_AFTER_LOGIN=self.after_login_url
        )
        if self.after_logout_url:
            app.config.update(
                CAS_AFTER_LOGOUT=self.after_logout_url,
            )

        CAS.__init__(self, app, '/cas')

    def current_user(self):
        user_id = self.user_id()
        if not self.user:
            user = {
                "email": self.user_email(),
                "roles": self.user_roles()
            }
            self.user = User(self.user_dao, user_id, user)
        return self.user

    def user_id(self):
        user_id = None
        if self.attributes:
            user_id = self.attributes.get("cas:_id", None)
        if isinstance(user_id, str):
            return user_id.lower()
        return user_id

    def user_email(self):
        email = None
        if 'CAS_USERNAME' in session:
            email = session.get("CAS_USERNAME")
            email = email.replace("MongoProfile#", "")
        return email

    def authenticate_anonymous_user(self, admin=False):
        session.update({
            "CAS_USERNAME": "anonymous"
        })
        if admin:
            session.update({"cas:roles": [PianoRoles.admin.value]})

    def admin_user(self):
        admin = False
        if self.token or session.get("CAS_USERNAME") == "anonymous":
            if PianoRoles.admin.value in self.user_roles():
                admin = True
        return admin

    def login_required(self, func):
        return cas_login_required(func)

    def logout(self):
        for k in list(session.keys()):
            del session[k]
            # if k.startswith("CAS") or k.startswith("_CAS"):
            #     session.pop(k)
        return cas_logout()

    def user_roles(self):
        roles = []
        if self.attributes:
            roles = self.attributes.get("cas:roles", [])
            if type(roles) is not list:
                roles = [roles]
        if session.get("CAS_USERNAME") == "anonymous":
            anon_roles = session.get("cas:roles", [])
            if anon_roles:
                roles += anon_roles
        return roles
