
import piano_auth

PIANO_AUTH_REQUIRED_CONFIGS = ["PIANO_AUTH_TYPE", "PIANO_AUTH_IDP_URLS"]
PIANO_AUTH_TYPES = ["CAS", "SAML", "KEYCLOAK_OPENID"]


class PianoAuthConfigException(Exception):
    def __init__(self, message="An error occurred trying to access required PianoAuth config value. "
                               "The following configs are required: %s" % ", ".join(PIANO_AUTH_REQUIRED_CONFIGS)):
        self.message = message


def piano_authentication(app, auth_type=None, *args, **kwargs):
    if auth_type is None and "PIANO_AUTH_TYPE" in app.config:
        auth_type = app.config["PIANO_AUTH_TYPE"]

    if auth_type == "KEYCLOAK_OPENID":
        return piano_auth.PianoKeycloakOpenID(app, *args, **kwargs)
    if auth_type == "CAS":
        return piano_auth.PianoCAS(app, *args, **kwargs)
    if auth_type == "SAML":
        return piano_auth.PianoSAML(app, *args, **kwargs)
    raise PianoAuthConfigException(f"Invalid PIANO_AUTH_TYPE '{auth_type}'. Must be one of {', '.join(PIANO_AUTH_TYPES)}")
