from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='piano_auth',
    packages=find_packages(exclude=["tests"]),
    version='1.2.4',
    description='Piano Auth',
    author='Evidentli',
    author_email='infrastructure@evidentli.com',
    url='https://bitbucket.org/evidentli/pyano-auth',
    license='MIT',
    keywords=['piano', 'util', 'auth', 'cas', 'saml', 'okta', 'keycloak', 'openid'],
    classifiers=[],
    install_requires=requirements,
    python_requires='>=3.9'
)
