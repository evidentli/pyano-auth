from unittest import TestCase
from flask import Flask
from piano_auth.authentication import PianoAuthConfigException, piano_authentication
from piano_auth.piano_cas import PianoCAS, CAS
from piano_auth.piano_saml import PianoSAML, LoginManager
from piano_auth.piano_keycloak import PianoKeycloakOpenID


def get_flask_app(auth_type=None, idp_urls=None):
    app = Flask(__name__)
    if auth_type:
        app.config["PIANO_AUTH_TYPE"] = auth_type
    if idp_urls:
        app.config["PIANO_AUTH_IDP_URLS"] = idp_urls
    return app


class TestPianoAuthorisation(TestCase):

    def test_init__with_invalid_type(self):
        """
        test initialisation of PianoAuth object with invalid type
        :return:
        """
        self.assertRaises(PianoAuthConfigException, piano_authentication, get_flask_app())
        self.assertRaises(PianoAuthConfigException, piano_authentication, get_flask_app(), "OKTA")


class TestSAMLAuthorisation(TestCase):

    def test_init(self):
        """
        test initialisation of PianoAuth object
        :return:
        """
        auth = piano_authentication(get_flask_app(), "SAML", {"name", "url"})
        self.assertTrue(isinstance(auth, PianoSAML))
        self.assertTrue(isinstance(auth, LoginManager))

        auth = piano_authentication(get_flask_app("SAML", {"name", "url"}))
        self.assertTrue(isinstance(auth, PianoSAML))
        self.assertTrue(isinstance(auth, LoginManager))

    def test_idp_urls(self):
        """
        test setting of idp_urls value
        :return:
        """
        idp_urls = {"mq": "http://okta.mq.edu", "piano": "http://piano.com"}

        auth = piano_authentication(get_flask_app(), "SAML", idp_urls)
        self.assertEqual(auth.idp_urls, idp_urls)

        auth = piano_authentication(get_flask_app("SAML", idp_urls))
        self.assertEqual(auth.idp_urls, idp_urls)

        self.assertRaises(PianoAuthConfigException, piano_authentication, get_flask_app(), "SAML")

    def test_saml_client__with_invalid_idp_urls(self):
        """
        test call to saml_client method when invalid idp_urls value has been set
        :return:
        """
        auth = piano_authentication(get_flask_app(), "SAML", {"piano": "http://piano.com"})
        self.assertRaises(Exception, auth.saml_client_for, "mq")


class TestCASAuthorisation(TestCase):

    def test_init(self):
        """
        test initialisation of PianoAuth object
        :return:
        """
        auth = piano_authentication(get_flask_app(), "CAS", {"cas": "url"})
        self.assertTrue(isinstance(auth, PianoCAS))
        self.assertTrue(isinstance(auth, CAS))

        auth = piano_authentication(get_flask_app("CAS", {"cas": "url"}))
        self.assertTrue(isinstance(auth, PianoCAS))
        self.assertTrue(isinstance(auth, CAS))

    def test_idp_urls(self):
        """
        test setting of idp_urls value
        :return:
        """
        idp_urls = {"cas": "url", "piano": "http://piano.com"}

        auth = piano_authentication(get_flask_app(), "CAS", idp_urls)
        self.assertEqual(auth.idp_urls, idp_urls)

        auth = piano_authentication(get_flask_app("CAS", idp_urls))
        self.assertEqual(auth.idp_urls, idp_urls)

        self.assertRaises(PianoAuthConfigException, piano_authentication, get_flask_app(), "CAS")


class TestKeycloakOpenIdAuthorisation(TestCase):

    def test_init(self):
        """
        test initialisation of PianoAuth object
        :return:
        """
        auth = piano_authentication(get_flask_app(), "KEYCLOAK_OPENID", {"keycloak": "url"})
        self.assertTrue(isinstance(auth, PianoKeycloakOpenID))

        auth = piano_authentication(get_flask_app("KEYCLOAK_OPENID", {"keycloak": "url"}))
        self.assertTrue(isinstance(auth, PianoKeycloakOpenID))

    def test_idp_urls(self):
        """
        test setting of idp_urls value
        :return:
        """
        idp_urls = {"keycloak": "url", "piano": "http://piano.com"}

        auth = piano_authentication(get_flask_app(), "KEYCLOAK_OPENID", idp_urls)
        self.assertEqual(auth.idp_urls, idp_urls)

        auth = piano_authentication(get_flask_app("KEYCLOAK_OPENID", idp_urls))
        self.assertEqual(auth.idp_urls, idp_urls)

        self.assertRaises(PianoAuthConfigException, piano_authentication, get_flask_app(), "CAS")
