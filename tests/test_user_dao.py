import os
from unittest import TestCase
from pymongo import MongoClient
from piano_users.user_dao import MongoUserDao, UserDaoException, PianoUserDao


PIANO_API = os.getenv("PIANO_API")
MONGO_HOST = os.getenv("MONGO_HOST", "localhost")
MONGO_PORT = int(os.getenv("MONGO_PORT", "27017"))
MONGO_DB = os.getenv("MONGO_DB", "pianousers")


class TestMongoUserDao(TestCase):

    dao = None
    table = None

    test_users = [
        {"_id": "troy.b@greendale.com",
         "email": "troy.b@greendale.com",
         "first_name": "",
         "last_name": "",
         "roles": ["student"]},
        {"_id": "annie.e@greendale.com",
         "email": "annie.e@greendale.com",
         "first_name": "Annie",
         "last_name": "Edison",
         "roles": ["student", "teacher"]},
    ]

    @classmethod
    def setUpClass(cls):
        cls.dao = MongoUserDao(MONGO_HOST, MONGO_PORT, MONGO_DB)

        # create test data
        client = MongoClient(MONGO_HOST, MONGO_PORT, connect=False)
        db = client[MONGO_DB]
        cls.table = db.users

        for user in cls.test_users:
            r = cls.table.insert_one(user)
            user["_id"] = r.inserted_id

    @classmethod
    def tearDownClass(cls):
        # clean up test data
        for user in cls.test_users:
            cls.table.delete_one({"_id": user["_id"]})

    def test_get_user(self):
        """
        test get each test user
        :return:
        """
        for test_user in self.test_users:
            user = self.dao.get_user(test_user["_id"])
            self.assertEqual(user, test_user)

    def test_get_user_not_exists(self):
        """
        test get non-existent user
        :return:
        """
        user = self.dao.get_user("xyz")
        self.assertFalse(user, "Found user that shouldn't exist")

    def test_update_create_user(self):
        """
        test create user using update function
        :return:
        """
        new_user = {"_id": "craig.p@greendale.com",
                    "email": "craig.p@greendale.com",
                    "first_name": "Craig",
                    "last_name": "Pelton",
                    "roles": "dean"}
        user_id = self.dao.update_user(new_user)
        self.assertEqual(user_id, new_user["_id"])
        self.test_users.append(new_user)
        self.assertEqual(self.dao.get_user(new_user["_id"]), new_user)

    def test_update_user(self):
        """
        test update user
        :return:
        """
        self.test_users[0]["roles"] = ["ac", "student"]
        user_id = self.dao.update_user(self.test_users[0])
        self.assertEqual(user_id, self.test_users[0]["_id"])
        self.assertEqual(self.test_users[0], self.dao.get_user(self.test_users[0]["_id"]))

    def test_update_user_invalid_data(self):
        """
        test update user with invalid data
        :return:
        """
        with self.assertRaises(UserDaoException):
            self.dao.update_user({"_id": self.test_users[0]["_id"], "$:hello": "world"})

    def test_update_user_invalid_no_id(self):
        """
        test update user without an _id
        :return:
        """
        with self.assertRaises(UserDaoException):
            self.dao.update_user({"email": "starburns@greendale.com", "first_name": "Star", "last_name": "Burns"})


class TestPianoUserDao(TestMongoUserDao):

    @classmethod
    def setUpClass(cls):
        super(TestPianoUserDao, cls).setUpClass()

        cls.dao = PianoUserDao()
